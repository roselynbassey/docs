include::ROOT:partial$attributes.adoc[]

= Marketing Resources
These resources will help you take Fedora to the people.


[[contact]]
== Where to find the Fedora Marketing Team

* link:{team_synch_communication}[Our room on Matrix]
* link:{team_asynch_communication}[Our tag on Fedora Discussion]
* link:{team_issue_tracker}/-/issues[Our planning discussions on GitLab]

The best way to find and contact the Marketing team is on Matrix/Element or by opening a ticket on GitLab.


[[social-media]]
== The social media profiles we currently manage

* https://fosstodon.org/@fedora[Mastodon]
* https://www.instagram.com/thefedoraproject/[Instagram]
* https://twitter.com/fedora[Twitter]
* https://www.linkedin.com/company/fedora-project/[LinkedIn]
* https://www.youtube.com/@fedora/[YouTube]

Don't see your favorite social media platform here?
*Help us spread the word by joining as a social media manager or helping create content for it!*


[[tools]]
== The tools we use in our work

* We use https://hackmd.io/[HackMD] for working in collaboration in our notes;
* We use both https://design.penpot.app[Penpot] and https://inkscape.org/[Inkscape] for our graphic design work.
They are the same tools the Fedora Design Team uses, so you can also help the Design Team out by learning to use our tools.

// * https://fedoraproject.org/wiki/Marketing/Talking_Points_Topics[Talking Points on Specific Topics]
// * [[Fedora press material| Fedora press material]]
// * [[Fedora press archive|  Fedora press archive]] 
// * [[F{{FedoraVersionNumber|current}}_screenshots_library|  Fedora F{{FedoraVersionNumber|current}} Screenshots]]
// * [[Presentations|  Fedora presentations]]
// * [[Marketing Fedora flyer|  Fedora flyers]] 
// * [[Design/SXSW_Materials#Brochures | Flyers/Brochures designed for SXSW]]
// * [[Artwork/PromoBanners|  Fedora banners from the Design team]] 
// * [[Artwork/MarketingCollateral|  Fedora T-Shirts, stickers, userbars, buttons and posters designs from the Design team]] 
// * [[Marketing statistics poster|  Fedora statistics poster]] 
// * [[Marketing social networks|  Social Networks]]
// * [[Link tracking| Link tracking]] for generating URLs that can be tracked non-invasively from logs
// * [[/MarketingPlan]]
// * [[Communication matrix]]
// * [[Videos]]
// * For the more PR-oriented, a [[Ambassadors/NA/Draft/NewsMedia_HowTo|News Media Howto]] and the [[Fedora_news_distribution_network_%28NDN%29|Fedora News Distribution Network]].
